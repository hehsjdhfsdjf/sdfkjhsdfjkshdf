package com.estefanpeixoto.icsfinal;

import java.io.File;
import java.io.FileWriter;

/**
 * A type to create transaction records and write them to a rudimentary database.
 */
class TransactionRecord {
    /**
     * The original balance.
     */
    double ogBal;
    /**
     * The new balance.
     */
    double newBal;
    /**
     * The difference.
     */
    double diff;
    /**
     * The account number.
     */
    int accNum;
    /**
     * Which account this transaction was for.
     */
    String whichAccount;
    /**
     *  Withdraw or deposit.
     */
    String withdrawOrDeposit;
    /**
     * If this was a manager transaction or not.
     */
    String managerTransaction;

    /**
     * creates a new transaction record.
     *
     * @param diff   the difference
     * @param accNum the account number
     */
    public TransactionRecord(double diff, int accNum) {
        this.diff = diff;
        this.accNum = accNum;
    }

    /**
     * Formats the record to be written to the database
     * @return the transaction record in CSV format
     */
    public String toString() {
        return ogBal + ", " + newBal + ", " + diff + ", " + accNum + ", " + withdrawOrDeposit + ", " + whichAccount + ", " + managerTransaction;
    }

    /**
     * Writes the transaction record to the CSV.
     */
    public void writeToRecord() {
        try {
            File ledger = new File("transactionrecord.csv");
            FileWriter wcsv = new FileWriter(ledger);

            wcsv.append(this.toString());

            wcsv.close();
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    /**
     * Sets if this is a manager transaction.
     *
     * @param admin true if it's an admin transaction
     */
    public void setManagerTransaction(boolean admin) {
        this.managerTransaction = (admin) ? "YES" : "NO";
    }

    /**
     * Sets which account.
     *
     * @param which true if the transaction is for the chequing account, false if it's for savings
     */
    public void setWhichAccount(boolean which) {
        this.whichAccount = (which) ? "CHEQUING" : "SAVINGS";
    }

    /**
     * Sets withdraw or deposit.
     *
     * @param which true if it's a deposit, false if it's a withdrawal
     */
    public void setWithdrawOrDeposit(boolean which) {
        this.withdrawOrDeposit = (which) ? "DEPOSIT" : "WITHDRAW";
    }

    /**
     * Sets balances.
     *
     * @param ogBal the original balance
     */
    public void setBalances(double ogBal) {
        this.ogBal = ogBal;

        if (this.withdrawOrDeposit.equals("DEPOSIT")) this.newBal += this.diff;
        else this.newBal -= this.diff;
    }
}