package com.estefanpeixoto.icsfinal;

import java.io.File;
import java.util.Scanner;

/**
 * The ATM interface.
 */
class TellerMachine {
    private static int currentAccount;
    private static String currentPin;
    /**
     * The main database of accounts.
     */
    public static BankAccount[] database = BankAccount.importAccounts();

    /**
     * Driver code.
     *
     * @param args boilerplate
     */
    public static void main(String[] args) {
        while (true) {
            int state = logon();
            switch (state) {
                case -1: {
                    System.out.println("Mismatched PIN, try again.");
                    break;
                }

                case 0: {
                    adminMenu();
                    break;
                }

                default: {
                    clientMenu();
                    break;
                }

            }
        }
    }

    /**
     * Simple login page
     *
     * @return either the client account number or -1 if there's a PIN mismatch
     */
    public static int logon() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("What's your account number? ");
        currentAccount = Integer.parseInt(scanner.nextLine());

        System.out.print("\nWhat is your PIN? ");
        currentPin = scanner.nextLine();

        if (BankAccount.isCorrectPin(currentPin, currentAccount)) return currentAccount;
        else return -1;
    }

    /**
     * Client menu.
     */
    public static void clientMenu() {
        Scanner scanner = new Scanner(System.in);
        boolean firstRun = true;
        while (true) {
            System.out.println("1 - Deposit | 2 - Withdraw | 3 - Get balance | 4 - Transfer");
            System.out.println("–1 - Exit.");
            int menu = scanner.nextInt();
            switch (menu) {
                case 1: {
                    deposit();
                }

                case 2: {
                    withdraw();
                }

                case 3: {
                    getBalance();
                }

                case 4: {
                    transfer();
                }

                case -1: {
                    if (!firstRun) BankAccount.updateCSV();
                    return;
                }

                default: {
                    if (firstRun) firstRun = false;
                }
            }
        }
    }

    /**
     * Admin menu.
     */
    public static void adminMenu() {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("1 - Deposit | 2 - Withdraw | 3 - Get balance | 4 - Transfer");
            System.out.println("5 - Create new account | 6 - Remove account | 7 - Reset PIN | 8 - Read transaction history");
            System.out.println("9 - See list of clients");
            System.out.println("-1 - Exit.");

            int menu = scanner.nextInt();

            switch (menu) {
                case 1: {
                    deposit(currentPin);
                    continue;
                }

                case 2: {
                    withdraw(currentPin);
                    continue;
                }

                case 3: {
                    getBalance(currentPin);
                    continue;
                }

                case 4: {
                    transfer(currentPin);
                    continue;
                }

                case 5: {
                    createAccount();
                    continue;
                }

                case 6: {
                    deleteAccount();
                    continue;
                }

                case 7: {
                    resetPin();
                    continue;
                }

                case 8: {
                    ledger();
                    continue;
                }

                case 9: {
                    clientList();
                    continue;
                }

                case -1: {
                    BankAccount.updateCSV();
                    return;
                }
            }
        }
    }

    /**
     * Deposit.
     */
    public static void deposit() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("How much will you be depositing? $");
        double amount = scanner.nextDouble();

        scanner.nextLine();

        System.out.print("\nWhich account are you depositing to? (CHEQUING, SAVINGS) ");
        boolean which = (scanner.nextLine().equals("CHEQUING"));

        BankAccount.deposit(currentAccount, Integer.parseInt(currentPin), amount, which, "");
    }

    /**
     * Withdraw.
     */
    public static void withdraw() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("How much will you be withdrawing? $");
        double amount = scanner.nextDouble();

        scanner.nextLine();

        System.out.print("\nWhich account are you withdrawing from? (CHEQUING, SAVINGS) ");
        boolean which = (scanner.nextLine().equals("CHEQUING"));

        BankAccount.withdraw(currentAccount, Integer.parseInt(currentPin), amount, which, "");
    }

    /**
     * Gets balance.
     */
    public static void getBalance() {
        BankAccount.getBal(currentAccount, Integer.parseInt(currentPin), "");
    }

    /**
     * Transfer.
     */
    public static void transfer() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("How much will you be transferring? $");
        double amount = scanner.nextDouble();

        scanner.nextLine();

        System.out.print("\nWhich account are you transferring from? (CHEQUING, SAVINGS) ");
        boolean swhich = (scanner.nextLine().substring(0, 1).equalsIgnoreCase("C"));

        System.out.print("\nWho is the recipient?");
        int receiver = scanner.nextInt();

        System.out.print("\nWhich account are you transferring to? (CHEQUING, SAVINGS) ");
        boolean rwhich = (scanner.nextLine().substring(0, 1).equalsIgnoreCase("C"));

        BankAccount.transfer(currentAccount, Integer.parseInt(currentPin), receiver, amount, swhich, rwhich, "");
    }

    /*
                     .
              \  :  /
               ' _ '
           -= ( (_) ) =-
               .   .
              /  :  \
          .-.    '
          |.|
        /)|`|(\
       (.(|'|)`)
    ~~~~`\`'./'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    jgs   |.|           ~~
          |`|                            ~~
         ,|'|.      (_)          ~~
          "'"        \"\
               ~~     ^~^
     */

    /**
     * Deposit to a client's account.
     *
     * @param admin overloading for admin method
     */
    public static void deposit(String admin) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter account number for the client you are operating on: ");
        int thisClient = scanner.nextInt();

        System.out.print("How much will you be depositing? $");
        double amount = scanner.nextDouble();

        scanner.nextLine();

        System.out.print("\nWhich account are you depositing to? (CHEQUING, SAVINGS) ");
        boolean which = (scanner.nextLine().substring(0, 1).equalsIgnoreCase("C"));

        BankAccount.deposit(thisClient, 0, amount, which, currentPin);
    }

    /**
     * Withdraw.
     *
     * @param admin overloading for admin method
     */
    public static void withdraw(String admin) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter account number for the client you are operating on: ");
        int thisClient = scanner.nextInt();

        System.out.print("How much will you be withdrawing? $");
        double amount = scanner.nextDouble();

        scanner.nextLine();

        System.out.print("\nWhich account are you withdrawing from? (CHEQUING, SAVINGS) ");
        boolean which = (scanner.nextLine().substring(0, 1).equalsIgnoreCase("C"));

        BankAccount.withdraw(thisClient, 0, amount, which, currentPin);
    }

    /**
     * Gets balance.
     *
     * @param admin overloading for admin method
     */
    public static void getBalance(String admin) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter account number for the client you are operating on: ");
        int thisClient = scanner.nextInt();

        BankAccount.getBal(thisClient, 0, currentPin);
    }

    /**
     * Transfer.
     *
     * @param admin overloading for admin method
     */
    public static void transfer(String admin) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter account number for the client you are operating on: ");
        int thisClient = scanner.nextInt();

        System.out.print("How much will you be transferring? $");
        double amount = scanner.nextDouble();

        scanner.nextLine();

        System.out.print("\nWhich account are you transferring from? (CHEQUING, SAVINGS) ");
        boolean swhich = (scanner.nextLine().substring(0, 1).equalsIgnoreCase("C"));

        System.out.print("\nWho is the recipient?");
        int receiver = scanner.nextInt();

        scanner.nextLine();

        System.out.print("\nWhich account are you transferring to? (CHEQUING, SAVINGS) ");
        boolean rwhich = (scanner.nextLine().substring(0, 1).equalsIgnoreCase("C"));

        BankAccount.transfer(thisClient, 0, receiver, amount, swhich, rwhich, currentPin);
    }

    /**
     * Creates an account.
     */
    public static void createAccount() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("What is the client's name? ");
        String name = scanner.nextLine();

        System.out.print("\nWhat will their PIN be? Any four digit number is acceptable. ");
        int pin = scanner.nextInt();

        System.out.print("\nInitial deposit into chequing: $");
        double cinit = scanner.nextDouble();

        System.out.println("Initial deposit into savings: $");
        double sinit = scanner.nextDouble();

        database[database.length - 1] = new BankAccount(pin, name, cinit, sinit);

        BankAccount[] newdb = new BankAccount[database.length + 1];

        System.arraycopy(database, 0, newdb, 0, database.length);

        database = newdb;

        BankAccount.updateCSV();
    }

    /**
     * Closes an account.
     */
    public static void deleteAccount() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter account number for the client you are operating on: ");
        int thisClient = scanner.nextInt();

        database[thisClient].closeAccount();

        BankAccount.updateCSV();
    }

    /**
     * Lists transactions.
     */
    public static void ledger() {
        try {
            File ledger = new File("ledger.csv");

            Scanner rcsv = new Scanner(ledger);

            while (rcsv.hasNextLine()) {
                System.out.println(rcsv.nextLine());
            }

            rcsv.close();
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    /**
     * Lists clients.
     */
    public static void clientList() {
        try {
            File ledger = new File("database.csv");

            Scanner rcsv = new Scanner(ledger);

            while (rcsv.hasNextLine()) {
                System.out.println(rcsv.nextLine());
            }

            rcsv.close();
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    /**
     * Reset pin.
     */
    public static void resetPin() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter account number for the client you are operating on: ");
        int thisClient = scanner.nextInt();

        System.out.print("What is the new PIN? ");
        int newPin = scanner.nextInt();

        BankAccount.forgotPin(thisClient, newPin);
    }
}