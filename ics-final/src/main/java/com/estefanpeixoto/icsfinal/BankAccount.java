package com.estefanpeixoto.icsfinal;

import java.io.*;
import java.util.Scanner;

/**
 * A class that creates a type BankAccount, for managing bank accounts, and facilitates recording the accounts in a rudimentary database.
 */
public class BankAccount {
    private int pin;
    private final int accountNumber;
    private String name;
    private double chequingBalance, savingsBalance;
    private static int NUMBER_OF_ACCOUNTS = 0;
    private static final String ADMIN_PASSWORD = "hci2022";

    private static final String SEPERATOR = ", ";
    private static final String NEWLINE = "\n";

    /**
     * Creates a new bank account.
     *
     * @param pin   the pin
     * @param name  the name
     * @param chBal the chequing balance
     * @param saBal the savings balance
     */
    public BankAccount(int pin, String name, double chBal, double saBal) {
        this.pin = pin;
        this.name = name;
        this.chequingBalance = chBal;
        this.savingsBalance = saBal;

        accountNumber = NUMBER_OF_ACCOUNTS + 1;

        NUMBER_OF_ACCOUNTS++;

        writeToRecord("Opened", name, this.accountNumber);
    }

    /**
     * Creates a new empty bank account.
     */
    public BankAccount() {
        this.pin = -999;
        this.name = "Null";
        this.chequingBalance = -999;
        this.savingsBalance = -999;
        this.accountNumber = 0;
    }

    /**
     * Deposits funds into the specified account.
     *
     * @param account       the account
     * @param pin           the account pin
     * @param dollarAmount  the amount to deposit
     * @param which         true = chequing account, false = savings account
     * @param adminPassword the admin password, if applicable
     */
    public static void deposit(int account, int pin, double dollarAmount, boolean which, String adminPassword) {
        TransactionRecord thisRecord = new TransactionRecord(dollarAmount, account);

        thisRecord.setWithdrawOrDeposit(true);

        if (adminPassword.equals(ADMIN_PASSWORD)) {
            pin = TellerMachine.database[account].pin;
            thisRecord.setManagerTransaction(true);
        }

        if (pin != TellerMachine.database[account].pin) throw new RuntimeException("Incorrect PIN.");
        else if (dollarAmount < 0)
            throw new RuntimeException("You cannot deposit a negative amount. Please use withdraw instead.");

        thisRecord.setWhichAccount(which); // True if chequing, false if savings

        if (which) {
            thisRecord.setBalances(TellerMachine.database[account].chequingBalance);
            TellerMachine.database[account].chequingBalance += dollarAmount;
        } else {
            thisRecord.setBalances(TellerMachine.database[account].savingsBalance);
            TellerMachine.database[account].savingsBalance += dollarAmount;
        }

        System.out.println("Deposited $" + dollarAmount + " successfully.");

        thisRecord.writeToRecord();
    }

    /**
     * Withdraw.
     *
     * @param account       the account
     * @param pin           the account pin
     * @param dollarAmount  the amount to withdraw
     * @param which         true = chequing account, false = savings account
     * @param adminPassword the admin password, if applicable
     */
    public static void withdraw(int account, int pin, double dollarAmount, boolean which, String adminPassword) {
        dollarAmount *= -1.0;

        TransactionRecord thisRecord = new TransactionRecord(dollarAmount, TellerMachine.database[account].accountNumber);

        thisRecord.setWithdrawOrDeposit(false);

        if (adminPassword.equals(ADMIN_PASSWORD)) {
            pin = TellerMachine.database[account].pin;
            thisRecord.setManagerTransaction(true);
        }

        if (pin != TellerMachine.database[account].pin) throw new RuntimeException("Incorrect PIN.");
        else if (dollarAmount > 0)
            throw new RuntimeException("You cannot withdraw a negative amount. Please use deposit instead.");

        thisRecord.setWhichAccount(which); // True if chequing, false if savings

        if (which) {
            if ((dollarAmount * -1.0) > TellerMachine.database[account].chequingBalance)
                throw new RuntimeException("You cannot take out more money than you have.");
            else {
                thisRecord.setBalances(TellerMachine.database[account].chequingBalance);
                TellerMachine.database[account].chequingBalance += dollarAmount;
            }
        } else {
            if ((dollarAmount * -1.0) > TellerMachine.database[account].savingsBalance)
                throw new RuntimeException("You cannot take out more money than you have.");
            else {
                thisRecord.setBalances(TellerMachine.database[account].savingsBalance);
                TellerMachine.database[account].savingsBalance += dollarAmount;
            }
        }

        System.out.println("Withdrew $" + dollarAmount + " successfully.");

        thisRecord.writeToRecord();
    }

    /**
     * Resets the PIN of a given account.
     *
     * @param account the account
     * @param newPin  the new pin
     */
    public static void forgotPin(int account, int newPin) {
        TellerMachine.database[account].pin = newPin;
        System.out.println("PIN has been successfully reset.");
    }

    public String toString() {
        return this.accountNumber + SEPERATOR + this.pin + SEPERATOR + this.name + SEPERATOR + this.chequingBalance + SEPERATOR + this.savingsBalance + NEWLINE;
    }

    /**
     * Imports bank accounts from database.csv
     *
     * @return the database of bank accounts, as an array of type BankAccount
     */
    public static BankAccount[] importAccounts() {
        BankAccount admin = new BankAccount();
        admin.name = "Admin";

        int csvLines = 0;

        /*
        First loop through database file: this gets the amount of accounts
         */

        try {
            File database = new File("database.csv");

            Scanner rcsv = new Scanner(database);

            while (rcsv.hasNextLine()) {
                rcsv.nextLine();
                csvLines++;
            }

            rcsv.close();
        } catch (Exception e) {
            e.getStackTrace();
        }

        BankAccount[] maindb = new BankAccount[csvLines + 2]; // create main database for program, one extra index for the admin user, buffer for adding an account

        maindb[0] = admin;

        /*
        This is where the import happens - it reads each line of the CSV and reads the account information from it, and adds it to the database array
        */

        try {
            int i = 1;

            File database = new File("database.csv");

            Scanner rcsv = new Scanner(database);

            while (rcsv.hasNextLine()) {
                maindb[i] = getAccountFromCSV(rcsv.nextLine());
                i++;
            }

            rcsv.close();
        } catch (Exception e) {
            e.getStackTrace();
        }

        return maindb;
    }

    /**
     * Reads a bank account formatted as CSV, and returns it as a BankAccount object
     *
     * @param line a bank account in CSV format
     * @return a BankAccount created from line
     */
    private static BankAccount getAccountFromCSV(String line) {
        BankAccount newAccount = new BankAccount(0, "", 0.0, 0.0);
        String[] details = line.split(", ");

        newAccount.pin = Integer.parseInt(details[1]);
        newAccount.name += details[2];
        newAccount.chequingBalance = Double.parseDouble(details[3]);
        newAccount.savingsBalance = Double.parseDouble(details[4]);

        return newAccount;
    }

    /**
     * Update database.csv with changes to accounts.
     */
    public static void updateCSV() {
        try {
            File ledger = new File("t_database.csv");

            if (ledger.createNewFile()) System.out.print("");

            FileWriter wcsv = new FileWriter(ledger);

            for (int i = 1; i < TellerMachine.database.length - 1; i++) {
                wcsv.append(TellerMachine.database[i].toString());
            }

            wcsv.close();

            File oldLedger = new File("database.csv");
            if (oldLedger.delete()) ledger.renameTo(oldLedger);
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    /**
     * Gets the balance of a given account.
     *
     * @param accountNumber the account number
     * @param pin           the pin
     * @param adminPassword the admin password, if applicable
     */
    public static void getBal(int accountNumber, int pin, String adminPassword) {
        if (adminPassword.equals(ADMIN_PASSWORD)) pin = TellerMachine.database[accountNumber].pin;
        if (pin != TellerMachine.database[accountNumber].pin) throw new RuntimeException("Incorrect PIN.");

        else {
            System.out.println("Your chequing balance is $" + TellerMachine.database[accountNumber].chequingBalance);
            System.out.println("Your savings balance is $" + TellerMachine.database[accountNumber].savingsBalance);
        }
    }

    /**
     * Is the given PIN correct?
     *
     * @param input         the PIN to check
     * @param accountNumber the account number to check it against
     * @return true if the PIN belongs to that account, false otherwise
     */
    public static boolean isCorrectPin(String input, int accountNumber) {
        if (accountNumber == 0) return input.equals(ADMIN_PASSWORD);
        else return Integer.parseInt(input) == TellerMachine.database[accountNumber].pin;
    }

    /**
     * Transfer between accounts.
     *
     * @param sender        the sender's account number
     * @param pin           the sender's pin
     * @param receiver      the receiver's account number
     * @param amount        the amount to transfer
     * @param swhich        from which of the sender's accounts (true for chequing, false for savings)
     * @param rwhich        to which of the receiver's accounts (true for chequing, false for savings)
     * @param adminPassword the admin password, if applicable
     */
    public static void transfer(int sender, int pin, int receiver, double amount, boolean swhich, boolean rwhich, String adminPassword) {
        if (adminPassword.equals(ADMIN_PASSWORD)) pin = TellerMachine.database[sender].pin;

        withdraw(sender, pin, amount, swhich, adminPassword);
        deposit(receiver, 0, amount, rwhich, ADMIN_PASSWORD);
    }

    /**
     * Closes a bank account.
     */
    public void closeAccount() {
        writeToRecord("Closed", this.name, this.accountNumber);

        chequingBalance = -999;
        savingsBalance = -999;
        pin = -999;
        name = "Former Client";
    }

    /**
     * Writes changes when new accounts are created or closed.
     *
     * @param type          if the account was opened or closed
     * @param name          the account holder's name
     * @param accountNumber the account number
     */
    public static void writeToRecord(String type, String name, int accountNumber) {
        try {
            File ledger = new File("clienthistory.csv");
            FileWriter wcsv = new FileWriter(ledger);

            wcsv.write(accountNumber + ", " + name + ", " + type);

            wcsv.close();
        } catch (Exception e) {
            e.getStackTrace();
        }
    }
}